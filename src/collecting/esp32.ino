#define TINY_GSM_MODEM_SIM800
#define SCAN_TIME 150

const char apn[]      = ""; // APN
const char gprsUser[] = ""; // GPRS User
const char gprsPass[] = ""; // GPRS Password

// SIM card PIN
const char simPIN[]   = "";

const char uuid       = "";
const char server[]   = "example.com"; // domain name
const char resource[] = "/post";
const int  port = 443;

#include <sstream>
#include <TinyGsmClient>
#include <ArduinoHttpClient>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

#ifdef DUMP_AT_COMMANDS
#include <StreamDebugger.h>
StreamDebugger debugger(SerialAT, SerialMon);
TinyGsm        modem(debugger);
#else
TinyGsm        modem(SerialAT);
#endif

TinyGsmClientSecure client(modem);
HttpClient          http(client, server, port);

bool sendData(String data) {
  bool stateGPRS = modem.isGprsConnected();
  bool res = false;
  if (stateGPRS) {
    http.beginRequest();
    http.post(resource);
    http->sendHeader("Content-Type", "application/json");
    http->sendHeader("Content-Length", data.length());
    http->beginBody();
    http->print(data);
    http->endRequest();
    int statusCode = http->responseStatusCode();
    if (statusCode == t_http_codes::HTTP_CODE_OK)
    {
      Serial.println("Status code: " + String(statusCode));
      res = true;
    }
    else
    {
      Serial.println("Error code: " + String(statusCode));
    }
  }
  return res;
}

BLEScan *pBLEScan;

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      Serial.printf("Advertised Device: %s \n", advertisedDevice.toString().c_str());
    }
};

void setup() {
  TinyGsmAutoBaud(SerialAT, GSM_AUTOBAUD_MIN, GSM_AUTOBAUD_MAX);
  delay(6000);
  modem.restart();
  if (GSM_PIN && modem.getSimStatus() != 3) {
    modem.simUnlock(GSM_PIN);
  }

  SerialMon.print("Waiting for network...");
  if (!modem.waitForNetwork()) {
    SerialMon.println("E: fail");
    delay(10000);
    return;
  }
  SerialMon.println("I: success");

  if (modem.isNetworkConnected()) {
    SerialMon.println("I: Network connected");
  }
  if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
    SerialMon.println("E: fail");
    delay(10000);
    return;
  }
  SerialMon.println("I: success");
  if (modem.isGprsConnected()) {
    SerialMon.println("I: GPRS connected");
  }
  http.connectionKeepAlive();

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);
  pBLEScan->setInterval(0x50);
  pBLEScan->setWindow(0x30);
}

void loop() {
  BLEScanResults foundDevices = pBLEScan->start(SCAN_TIME);
  int count = foundDevices.getCount();
  std::stringstream ss;
  ss << "[";
  for (int i = 0; i < count; i++)
  {
    if (i > 0)
      ss << ",";

    BLEAdvertisedDevice d = foundDevices.getDevice(i);
    ss << "{\"Address\":\"" << d.getAddress().toString() << "\",\"Rssi\":" << d.getRSSI();

    if (d.haveName())
      ss << ",\"Name\":\"" << d.getName() << "\"";

    if (d.haveAppearance())
      ss << ",\"Appearance\":" << d.getAppearance();

    if (d.haveManufacturerData())
    {
      std::string md = d.getManufacturerData();
      uint8_t *mdp = (uint8_t *)d.getManufacturerData().data();
      char *pHex = BLEUtils::buildHexData(nullptr, mdp, md.length());
      ss << ",\"ManufacturerData\":\"" << pHex << "\"";
      free(pHex);
    }

    if (d.haveServiceUUID())
      ss << ",\"ServiceUUID\":\"" << d.getServiceUUID().toString() << "\"";

    if (d.haveTXPower())
      ss << ",\"TxPower\":" << (int)d.getTXPower();

    ss << "}";
  }
  ss << "]";

  int httpCode = sendData(ss.str().c_str());
  if (httpCode > 0)
  {
    log_i("[HTTP] POST... code: %d\n", httpCode);
    if (httpCode == HTTP_CODE_OK)
      log_i("[HTTP] Response:\n%s", http.getString());
  }
  else
  {
    log_i("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
  }
}
