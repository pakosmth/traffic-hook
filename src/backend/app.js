const express = require("express"),
  app = express();
const bodyParser = require("body-parser");
const ejs = require("ejs");
const path = require("path");

app.use(express.static(path.join(__dirname, "../frontend")));
app.set("views", path.join(__dirname, "../frontend/views"));
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));

const port = process.env.PORT || 3000;

app.get("/", (req, res) => {
  res.render("index");
});

app.listen(port, () => {
  console.log(`Server started at port ${port}`);
});
