ymaps.ready(init);
var myMap;
function init() {
  myMap = new ymaps.Map("map", {
    center: [55.75322, 37.622513],
    zoom: 10,
    controls: [],
  });

  var actualProvider = new ymaps.traffic.provider.Actual(
    {},
    { infoLayerShown: true }
  );
  actualProvider.setMap(myMap);

  var x = document.getElementsByClassName("ymaps-2-1-78-copyrights-pane")[0];
  x.parentNode.removeChild(x);

  setInterval(gen, Math.floor(Math.random() * 1000000) % 10000);
}

function gen() {
  var dot_type = Math.floor(Math.random() * 1000) % 9;
  var x =
    (Math.floor(Math.random() * 1000000) % 1500) *
    (Math.floor(Math.random() * 1000) % 2 ? 1 : -1);
  var y =
    (Math.floor(Math.random() * 1000000) % 2000) *
    (Math.floor(Math.random() * 1000) % 2 ? 1 : -1);
  console.log(dot_type);
  ymaps
    .geocode([55.754758 + x / 10000, 37.598521 + y / 10000], {
      kind: "street",
      results: 1,
    })
    .then(function (res) {
      var message = "";
      var haveReplies = Math.floor(Math.random() * 1000) % 2;
      var reply = "<center>Отзывы с Я.Карт</center>"
      if (dot_type) {
        res.geoObjects.options.set("preset", "islands#grayCircleIcon");
        message = "<h3>🔶 Вероятнее всего на данном участке пробка</h3>";
      } else {
        res.geoObjects.options.set("preset", "islands#redCircleIcon");
        message = "<h3>🔴 Вероятнее всего на данном участке произошло ДТП</h3>";
      }
      res.geoObjects.events
        .add("mouseenter", function (event) {
          var geoObject = event.get("target");
          var bodyText = geoObject.properties.get("balloonContentBody");
          bodyText += message;
          if(haveReplies) bodyText += reply;
          myMap.hint.open(geoObject.geometry.getCoordinates(), bodyText);
        })
        .add("mouseleave", function (event) {
          myMap.hint.close(true);
        });
      myMap.geoObjects.add(res.geoObjects);
    });
}
